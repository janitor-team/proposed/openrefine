openrefine (3.6.1-1) unstable; urgency=medium

  * New upstream version 3.6.1.
  * Refresh all patches except of javalamp patch.
  * Tighten dependency on apache-jena and wikidata toolkit.
  * Depend on liblanguage-detector-java.
  * Add gdata-extension.patch.
  * Declare compliance with Debian Policy 4.6.1.

 -- Markus Koschany <apo@debian.org>  Thu, 29 Sep 2022 23:58:11 +0200

openrefine (3.5.2-2) unstable; urgency=medium

  * Build-depend on libokhttp-java (>= 3.13.1-3~)
  * Tighten dependency on libgoogle-api-client-java.
  * Remove dependency on tomcat9 because the tomcat9-annotations-api is
    apparently not required.
  * Update the Dockerfile and add a README file to document how to build the
    image and run the container. Install both files as examples into
    /usr/share/doc/openrefine/examples.

 -- Markus Koschany <apo@debian.org>  Tue, 08 Mar 2022 13:49:15 +0100

openrefine (3.5.2-1) unstable; urgency=medium

  * Upload to unstable.
  * New upstream version 3.5.2.
   - Remove non-free lavalamp.js file.
   - Enable all extensions.
  * Depend on procps for openrefine script.

 -- Markus Koschany <apo@debian.org>  Sun, 20 Feb 2022 17:03:52 +0100

openrefine (3.5~git20210527-1) experimental; urgency=medium

  * Initial release. (Closes: #986604 )

 -- Markus Koschany <apo@debian.org>  Thu, 02 Sep 2021 06:56:05 +0200
