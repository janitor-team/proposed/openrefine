#!/usr/bin/make -f

LINK_JARS_SERVER := butterfly commons-collections3 commons-io commons-lang3
LINK_JARS_SERVER += jetty9-servlet jetty9-webapp jetty9-server metaweb-lessen
LINK_JARS_SERVER += jetty9-http jetty9-io jetty9-security jetty9-util jetty9-xml
LINK_JARS_SERVER += slf4j-api slf4j-log4j12 velocity servlet-api js rhino log4j-1.2
LINK_JARS_SERVER += oro

LINK_JARS_WEBAPP := ant ant-launcher openrefine-arithcode butterfly clojure
LINK_JARS_WEBAPP += dexx.collection commons-beanutils commons-cli commons-codec
LINK_JARS_WEBAPP += commons-collections3 commons-collections4 commons-compress
LINK_JARS_WEBAPP += commons-csv commons-digester commons-fileupload commons-io
LINK_JARS_WEBAPP += commons-lang3 commons-lang-2.6 commons-logging commons-math3 commons-text
LINK_JARS_WEBAPP += commons-validator guava httpclient httpclient-cache httpcore
LINK_JARS_WEBAPP += jackson-annotations jackson-core jackson-databind java-rdfa
LINK_JARS_WEBAPP += jaxb-api jcl-over-slf4j jena-arq jena-base jena-cmds jena-core
LINK_JARS_WEBAPP += jena-dboe-base jena-dboe-index jena-dboe-transaction jena-dboe-trans-data
LINK_JARS_WEBAPP += jena-iri jena-rdfconnection jena-tdb2 jena-tdb jena-shacl joda-time js rhino
LINK_JARS_WEBAPP += jsonld-java jsoup jsr305 juniversalchardet metaweb-lessen thrift log4j-1.2 log4j-core
LINK_JARS_WEBAPP += odfdom-java openrefine-opencsv poi poi-ooxml poi-ooxml-schemas
LINK_JARS_WEBAPP += ptk-common rats-runtime com.wcohen.secondstring servlet-api signpost-commonshttp4
LINK_JARS_WEBAPP += signpost-core slf4j-api slf4j-log4j12 swc-parser-lazy velocity
LINK_JARS_WEBAPP += vicino xercesImpl xml-apis xmlbeans language-detector
LINK_JARS_WEBAPP += httpclient5 httpcore5 httpcore5-h2 utils marc4j javax.activation
LINK_JARS_WEBAPP += jetty9-continuation jetty9-http jetty9-io jetty9-servlets jetty9-util

LINK_JARS_EXT_WIKIDATA := signpost-core okhttp-urlconnection okhttp wdtk-wikibaseapi threeten-extra
LINK_JARS_EXT_WIKIDATA += jackson-annotations commons-lang3 wdtk-datamodel commons-compress commons-codec
LINK_JARS_EXT_WIKIDATA += jackson-core okhttp-signpost okio wdtk-util jackson-databind

LINK_JARS_EXT_JYTHON := jython

LINK_JARS_EXT_GDATA := grpc-context opencensus-contrib-http-util commons-logging jsr305 httpclient
LINK_JARS_EXT_GDATA += google-oauth-client google-api-services-sheets httpcore google-api-services-drive
LINK_JARS_EXT_GDATA += google-api-client opencensus-api google-http-client-jackson2 google-http-client-gson
LINK_JARS_EXT_GDATA += commons-codec guava google-http-client

LINK_JARS_EXT_DATABASE := encoder grpc-context opencensus-contrib-http-util commons-logging jsr305 httpclient postgresql
LINK_JARS_EXT_DATABASE += httpcore jackson-annotations commons-lang3 opencensus-api google-http-client-jackson2 commons-codec
LINK_JARS_EXT_DATABASE += guava mariadb-java-client jasypt google-http-client xerial-sqlite-jdbc

%:
	dh $@

override_dh_install:
	find main/webapp/WEB-INF/ -name "*.jar" -delete
	find main/webapp/WEB-INF/ -name "*.java" -delete
	find server/classes/ -name "*.java" -delete
	find extensions/ -type f \( -iname "LICENSE*" -o -name "*.java" -o -name "*.jar" -o -name ".project" -o -name "*.settings" -o -name "pom.xml" \) -delete
	find extensions/ -type d -depth -name "tests" -exec rm -r "{}" \;
	find extensions/ -type d -depth -name "target" -exec rm -r "{}" \;
	find extensions/ -type d -depth -name "src" -exec rm -r "{}" \;
	find extensions/ -type d -depth -name "licenses" -exec rm -r "{}" \;
	dh_install
	$(RM) -r $(CURDIR)/debian/openrefine/usr/share/maven-repo

override_dh_link:
	for JAR in $(LINK_JARS_SERVER); do \
		dh_link /usr/share/java/$$JAR.jar /usr/share/openrefine/server/$$JAR.jar; \
	done
	for JAR in $(LINK_JARS_WEBAPP); do \
		dh_link /usr/share/java/$$JAR.jar /usr/share/openrefine/webapp/WEB-INF/lib/$$JAR.jar; \
	done
	for JAR in $(LINK_JARS_EXT_WIKIDATA); do \
		dh_link /usr/share/java/$$JAR.jar /usr/share/openrefine/webapp/extensions/wikidata/module/MOD-INF/lib/$$JAR.jar; \
	done
	for JAR in $(LINK_JARS_EXT_JYTHON); do \
		dh_link /usr/share/java/$$JAR.jar /usr/share/openrefine/webapp/extensions/jython/module/MOD-INF/lib/$$JAR.jar; \
	done
	for JAR in $(LINK_JARS_EXT_GDATA); do \
		dh_link /usr/share/java/$$JAR.jar /usr/share/openrefine/webapp/extensions/gdata/module/MOD-INF/lib/$$JAR.jar; \
	done
	for JAR in $(LINK_JARS_EXT_DATABASE); do \
		dh_link /usr/share/java/$$JAR.jar /usr/share/openrefine/webapp/extensions/database/module/MOD-INF/lib/$$JAR.jar; \
	done
